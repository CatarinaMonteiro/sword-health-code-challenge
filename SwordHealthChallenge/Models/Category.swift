//
//  Category.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 02/02/2022.
//

import Foundation

struct Category: Codable{
    let id: Int
    let name: String
}
