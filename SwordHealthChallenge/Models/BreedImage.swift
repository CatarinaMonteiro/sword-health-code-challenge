//
//  BreedImage.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 05/02/2022.
//

import Foundation

class BreedImage: Codable {
    let id: String
    let url: String
    let categories: [Category]?
    let breeds: [Breed]
}
