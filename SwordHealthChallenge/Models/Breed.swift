//
//  Breed.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 01/02/2022.
//

import Foundation

struct Breed: Codable {
    let id: String
    let breedName: String
    let temperament: String?
    let origin: String?
    let group: String?
    
    // for the breed group case I followed this example:
    // https://documenter.getpostman.com/view/4016432/the-dog-api/RW81vZ4Z#77124c23-dc8b-48f0-9cc2-7e009ecf74fe
    // However it is not working and I couldn't find this parameter on the api documentation
    
    
    private enum CodingKeys: String, CodingKey {
        case breedName = "name"
        case group = "breed_group"
        case id, temperament, origin
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        breedName = try container.decode(String.self, forKey: .breedName)
        do {
            id = try String(container.decode(Int.self, forKey: .id))
        } catch DecodingError.typeMismatch {
            id = try container.decode(String.self, forKey: .id)
        }
        temperament = try? container.decode(String?.self, forKey: .temperament)
        origin = try? container.decode(String?.self, forKey: .origin)
        group = try? container.decode(String?.self, forKey: .group)
    }
}

