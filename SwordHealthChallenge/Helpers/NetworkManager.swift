//
//  NetworkManager.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 01/02/2022.
//

import Foundation
import Alamofire

enum Endpoints: String {
    case breedsSearch = "https://api.thecatapi.com/v1/breeds/search"
    case images = "https://api.thedogapi.com/v1/images/search"
}

final class NetworkManager {
    
    let apiKey = "abc6ea10-a6b7-4836-a5c5-5c7bbda6d3f4"
    
    func getRequest<T: Decodable>(from: String, parameters: Parameters?, decodable: T.Type, completion:@escaping (_ details: [T]) -> Void) {
        
        let header: HTTPHeaders = [HTTPHeader(name: "x-api-key", value: apiKey)]
        
        AF.request(from, method: .get, parameters: parameters, headers: header)
            .response { response in
                
                switch response.result {
                case .success:
                    guard let res = response.data else {return}
                    do {
                        let decoder = JSONDecoder()
                        let data = try decoder.decode([T].self, from: res)
                        completion(data)
                    } catch {
                        print("Error", error)
                    }
                    
                case let .failure(error):
                    print(error)
                }
            }
    }
}
