//
//  DetailsViewController.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 31/01/2022.
//

import UIKit

final class DetailsViewController: UIViewController {
    
    //MARK: - Constants

    private let containerViewTopPaddingConstant: CGFloat = 80
    
    private let categoryNotAvailableLablelText = "Category Not Available"
    private let originNotAvailableLablelText = "Origin Not Available"
    private let temperamentNotAvailableLablelText = "Temperament Not Available"
    
    
    // MARK: - Subviews
    
    private let containerStackView = UIStackView()
    private let nameLabel = UILabel()
    private let categoryLabel = UILabel()
    private let originLabel = UILabel()
    private let temperamentLabel = UILabel()
    
    
    // MARK: - Properties
    
    private let breed: Breed
    private let category: Category?
    
    // MARK: - Lifecycle
    
    init(breed: Breed, category: Category? = nil){
        self.breed = breed
        self.category = category
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI(){
        
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        categoryLabel.translatesAutoresizingMaskIntoConstraints = false
        originLabel.translatesAutoresizingMaskIntoConstraints = false
        temperamentLabel.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(containerStackView)
        
        containerStackView.axis = .vertical
        containerStackView.distribution = .equalSpacing

        containerStackView.addArrangedSubview(nameLabel)
        containerStackView.addArrangedSubview(categoryLabel)
        containerStackView.addArrangedSubview(originLabel)
        containerStackView.addArrangedSubview(temperamentLabel)
        
        NSLayoutConstraint.activate([
            containerStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: containerViewTopPaddingConstant),
            containerStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        nameLabel.text = breed.breedName
        nameLabel.textAlignment = .center
        
        categoryLabel.text = category?.name ?? categoryNotAvailableLablelText
        categoryLabel.textAlignment = .center

        originLabel.text = breed.origin ?? originNotAvailableLablelText
        originLabel.textAlignment = .center

        temperamentLabel.text = breed.temperament ?? temperamentNotAvailableLablelText
        temperamentLabel.textAlignment = .center
    }
}
