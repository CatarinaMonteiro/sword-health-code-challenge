//
//  SearchCollectionViewCell.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 05/02/2022.
//

import Foundation
import UIKit

final class SearchCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Constants
    
    private let labelsHeightConstant: CGFloat = 20
    
    private let groupNotAvailableLablelText = "Group Not Available"
    private let originNotAvailableLablelText = "Origin Not Available"
    private let temperamentNotAvailableLablelText = "Temperament Not Available"
    
    //MARK: - Subviews
    
    private let nameLabel = UILabel()
    private let groupLabel = UILabel()
    private let originLabel = UILabel()
    private let temperamentLabel = UILabel()
    
    //MARK: - LifeCycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubviews(){
        
        self.isUserInteractionEnabled = true
        
        addSubview(nameLabel)
        addSubview(groupLabel)
        addSubview(originLabel)
        addSubview(temperamentLabel)
        
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        groupLabel.translatesAutoresizingMaskIntoConstraints = false
        originLabel.translatesAutoresizingMaskIntoConstraints = false
        temperamentLabel.translatesAutoresizingMaskIntoConstraints = false
        
        nameLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: topAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            nameLabel.heightAnchor.constraint(equalToConstant: labelsHeightConstant),
            
            groupLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor),
            groupLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            groupLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            groupLabel.heightAnchor.constraint(equalToConstant: labelsHeightConstant),
            
            originLabel.topAnchor.constraint(equalTo: groupLabel.bottomAnchor),
            originLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            originLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            originLabel.heightAnchor.constraint(equalToConstant: labelsHeightConstant),
            
            temperamentLabel.topAnchor.constraint(equalTo: originLabel.bottomAnchor),
            temperamentLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            temperamentLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            temperamentLabel.heightAnchor.constraint(equalToConstant: labelsHeightConstant)
        ])
    }
    
    func setupCell(breed: Breed) {
        nameLabel.text = breed.breedName
        groupLabel.text = breed.group ?? groupNotAvailableLablelText
        originLabel.text = breed.origin ?? originNotAvailableLablelText
        temperamentLabel.text = breed.temperament ?? temperamentNotAvailableLablelText
    }
    
    override func prepareForReuse() {
        nameLabel.text = nil
        groupLabel.text = nil
        originLabel.text = nil
        temperamentLabel.text = nil
    }
}
