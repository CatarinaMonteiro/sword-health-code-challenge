//
//  SearchViewController.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 31/01/2022.
//

import UIKit

final class SearchViewController: UIViewController {
    
    //MARK: - Constants
    
    private let sectionInsets = UIEdgeInsets(top: 0.0,
                                             left: 20.0,
                                             bottom: 50.0,
                                             right: 20.0)
    
    private let searchBarTopPaddingConstant: CGFloat = 80
    private let collectionViewPaddingConstant: CGFloat = 20
    private let cellHeightConstant: CGFloat = 100
    private let searchBarPlaceholder = "Type a breed name"
    private let cellIdentifier = "cell"
    
    //MARK: - Subviews
    
    private let breedsCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    private let searchBar = UISearchBar()
    
    //MARK: - Properties
    
    private var itemsPerRow: CGFloat = 1 {
        didSet{
            breedsCollectionView.reloadData()
        }
    }
    
    private var breeds: [Breed] = [] {
        didSet {
            breedsCollectionView.reloadData()
        }
    }
    
    private var currentPage: Int = 0
    private var currentStartIndex = 0
    
    private var previousRun = Date()
    private let minInterval = 0.05
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func performSearch(name: String) {
        let params = [
            "q": name
        ]
        
        let networking = NetworkManager()
        networking.getRequest(from: Endpoints.breedsSearch.rawValue, parameters: params, decodable: Breed.self) { res in
            self.breeds = res
        }
    }
    
    private func setupUI(){
        
        breedsCollectionView.dataSource = self
        breedsCollectionView.delegate = self
        breedsCollectionView.backgroundColor = .white
        breedsCollectionView.register(SearchCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.placeholder = searchBarPlaceholder
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        
        breedsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(searchBar)
        view.addSubview(breedsCollectionView)
        
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.topAnchor, constant: searchBarTopPaddingConstant),
            searchBar.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            breedsCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            breedsCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            breedsCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            breedsCollectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: collectionViewPaddingConstant)
        ])
    }
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        breeds.removeAll()
        guard let textToSearch = searchBar.text, !textToSearch.isEmpty else {
            return
        }
        
        if Date().timeIntervalSince(previousRun) > minInterval {
            previousRun = Date()
            performSearch(name: textToSearch)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        breeds.removeAll()
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return breeds.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellHeight: CGFloat = cellHeightConstant
        let cellWidth: CGFloat = self.breedsCollectionView.frame.width - collectionViewPaddingConstant
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? SearchCollectionViewCell else { return UICollectionViewCell() }
        
        cell.setupCell(breed: breeds[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsViewController = DetailsViewController(breed: breeds[indexPath.item])
        self.navigationController?.pushViewController(detailsViewController, animated: false)
    }
}
