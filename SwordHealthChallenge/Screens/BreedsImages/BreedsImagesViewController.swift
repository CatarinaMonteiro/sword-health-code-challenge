//
//  DogBreedsViewController.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 31/01/2022.
//

import UIKit

final class BreedsImagesViewController: UIViewController {
    
    //MARK: - Types
    
    private enum AlphabeticalOrder: String {
        case random = "random"
        case ascendent = "ascendent"
        case descendent = "descendent"
    }
    
    //MARK: - Constants
    
    private let sectionInsets = UIEdgeInsets(top: 50.0,
                                     left: 20.0,
                                     bottom: 50.0,
                                     right: 20.0)
    
    private let nrOfItemsSegmentedControlTopPaddingConstant: CGFloat = 80
    private let nrOfItemsSegmentedControlWidthConstant: CGFloat = 100
    private let nrOfItemsSegmentedControlHeightConstant: CGFloat = 40
    private let alphabeticalOrderSegmentedControlTopPaddingConstant: CGFloat = 20
    private let breedsCollectionViewTopPaddingConstant: CGFloat = 20
    
    private let imageNotAvailableLablelText = "Name Not Available"
    private let cellIdentifier = "cell"
    
    //MARK: - Subviews
    
    private let breedsCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    private let nrOfItemsSegmentedControl = UISegmentedControl(items: ["List", "Grid"])
    private let alphabeticalOrderSegmentedControl = UISegmentedControl(items: [AlphabeticalOrder.random.rawValue, AlphabeticalOrder.ascendent.rawValue, AlphabeticalOrder.descendent.rawValue])
    
    //MARK: - Properties
    
    private var itemsPerRow: CGFloat = 1 {
        didSet{
            breedsCollectionView.reloadData()
        }
    }
    
    private var images: [BreedImage] = [] {
        didSet {
            breedsCollectionView.reloadData()
        }
    }
    
    private var alphabeticOrder: AlphabeticalOrder = .random {
        didSet {
            loadMoreItems { [weak self] (images) in
                
                guard let self = self else { return }
                
                guard let images = images else { return }
                self.images = images
                self.currentPage += 1
                self.currentStartIndex = self.images.count
                self.breedsCollectionView.reloadData()
            }
        }
    }
    
    private var currentPage: Int = 0
    private var currentStartIndex = 0
    
    private let cache = NSCache<NSString, UIImage>()
    private let utilityQueue = DispatchQueue.global(qos: .utility)
    
    
    //MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadMoreItems { [weak self] (images) in
            
            guard let self = self else { return }
            
            guard let images = images else { return }
            self.images += images
            self.currentPage += 1
            self.breedsCollectionView.reloadData()
            self.currentStartIndex = self.images.count
            
            self.nrOfItemsSegmentedControl.isUserInteractionEnabled = true
            self.alphabeticalOrderSegmentedControl.isUserInteractionEnabled = true
        }
    }
        
    private func setupUI(){
        
        breedsCollectionView.dataSource = self
        breedsCollectionView.delegate = self
        breedsCollectionView.backgroundColor = .white
        breedsCollectionView.register(BreedsImagesCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        
        nrOfItemsSegmentedControl.isUserInteractionEnabled = false
        nrOfItemsSegmentedControl.selectedSegmentIndex = 0
        nrOfItemsSegmentedControl.addTarget(self, action: #selector(segmentedControlChanged(_:)), for: .valueChanged)
        
        alphabeticalOrderSegmentedControl.isUserInteractionEnabled = false
        alphabeticalOrderSegmentedControl.selectedSegmentIndex = 0
        alphabeticalOrderSegmentedControl.addTarget(self, action: #selector(sortSegmentedControlChanged(_:)), for: .valueChanged)
        
        breedsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        nrOfItemsSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        alphabeticalOrderSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(nrOfItemsSegmentedControl)
        view.addSubview(alphabeticalOrderSegmentedControl)
        view.addSubview(breedsCollectionView)
        
        NSLayoutConstraint.activate([
            nrOfItemsSegmentedControl.topAnchor.constraint(equalTo: view.topAnchor, constant: nrOfItemsSegmentedControlTopPaddingConstant),
            nrOfItemsSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            nrOfItemsSegmentedControl.widthAnchor.constraint(equalToConstant: nrOfItemsSegmentedControlWidthConstant),
            nrOfItemsSegmentedControl.heightAnchor.constraint(equalToConstant: nrOfItemsSegmentedControlHeightConstant),
            
            alphabeticalOrderSegmentedControl.topAnchor.constraint(equalTo: nrOfItemsSegmentedControl.bottomAnchor, constant: alphabeticalOrderSegmentedControlTopPaddingConstant),
            alphabeticalOrderSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            alphabeticalOrderSegmentedControl.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            alphabeticalOrderSegmentedControl.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            alphabeticalOrderSegmentedControl.heightAnchor.constraint(equalToConstant: nrOfItemsSegmentedControlHeightConstant),
            
            breedsCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            breedsCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            breedsCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            breedsCollectionView.topAnchor.constraint(equalTo: alphabeticalOrderSegmentedControl.bottomAnchor, constant: breedsCollectionViewTopPaddingConstant)
        ])
    }
    
    @objc
    private func segmentedControlChanged(_ sender: UISegmentedControl){
        switch sender.selectedSegmentIndex{
        case 0:
            itemsPerRow = 1
            self.breedsCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0),
                                                   at: .top,
                                                   animated: true)
        case 1:
            itemsPerRow = 2
            self.breedsCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0),
                                                   at: .top,
                                                   animated: true)
        default:
            break
        }
    }
    
    @objc
    private func sortSegmentedControlChanged(_ sender: UISegmentedControl){
        switch sender.selectedSegmentIndex{
        case 0:
            self.breedsCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0),
                                                   at: .top,
                                                   animated: true)
            alphabeticOrder = .random
        case 1:
            self.breedsCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0),
                                                   at: .top,
                                                   animated: true)
            alphabeticOrder = .ascendent
        case 2:
            self.breedsCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0),
                                                   at: .top,
                                                   animated: true)
            alphabeticOrder = .descendent
        default:
            break
        }
    }
    
    //MARK: - API Helper Methods
    
    private func loadMoreItems(completion: @escaping ([BreedImage]?) -> ()) {
        
        utilityQueue.async {
            
            var params: [String: Any] = [
                "limit": 20,
                "page": self.currentPage
            ]
            
            switch self.alphabeticOrder {
            case .random:
                params["order"] = "RANDOM"
            case .ascendent:
                params["order"] = "ASC"
            case .descendent:
                params["order"] = "DESC"
            }
            
            let networking = NetworkManager()
            networking.getRequest(from: Endpoints.images.rawValue, parameters: params, decodable: BreedImage.self) { res in
                DispatchQueue.main.async {
                    completion(res)
                }
            }
        }
    }
    
    private func loadImage(url: String, completion: @escaping (UIImage?) -> ()) {
        utilityQueue.async {
            guard let url = URL(string: url) else { return }
            
            guard let data = try? Data(contentsOf: url) else { return }
            let image = UIImage(data: data)
            
            DispatchQueue.main.async {
                completion(image)
            }
        }
    }
}

extension BreedsImagesViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func numberOfSectionsInCollectionView(collectionView:
                                          UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? BreedsImagesCollectionViewCell else { return UICollectionViewCell() }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let breed = images[indexPath.item].breeds.first else { return }
        let detailsViewController = DetailsViewController(breed: breed, category: images[indexPath.item].categories?.first)
        self.navigationController?.pushViewController(detailsViewController, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
        guard let cell = cell as? BreedsImagesCollectionViewCell, !self.images.isEmpty else { return }
        
        let itemID = NSString(string: self.alphabeticOrder.rawValue + indexPath.item.description)
        
        if let cachedImage = self.cache.object(forKey: itemID) {
            cell.setupCell(breedImage: cachedImage, breedName: self.images[indexPath.item].breeds.first?.breedName ?? imageNotAvailableLablelText)
        } else {
            self.loadImage(url: self.images[indexPath.item].url) { image in
                guard let image = image else { return }
                self.cache.setObject(image, forKey: itemID )
                cell.setupCell(breedImage: image, breedName: self.images[indexPath.item].breeds.first?.breedName ?? self.imageNotAvailableLablelText)
            }
        }
        
        let lastIndex = self.images.count - 1
        if indexPath.row == lastIndex {
            
            loadMoreItems{ [weak self] (images) in
                
                guard let self = self else { return }
                
                guard let images = images else { return }
                self.images += images
                self.currentPage += 1
                self.currentStartIndex = self.images.count
            }
        }
    }
}
