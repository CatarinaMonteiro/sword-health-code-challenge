//
//  BreedsCollectionViewCell.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 02/02/2022.
//

import Foundation
import UIKit

final class BreedsImagesCollectionViewCell: UICollectionViewCell {
    
    // MARK: - SubViews
    
    private let breedImage = UIImageView()
    private let breedName = UILabel()
    
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubviews(){
        
        self.isUserInteractionEnabled = true
        
        addSubview(breedImage)
        addSubview(breedName)
        
        breedImage.contentMode = .scaleAspectFit
        breedImage.clipsToBounds = true
        breedImage.translatesAutoresizingMaskIntoConstraints = false
        
        breedName.translatesAutoresizingMaskIntoConstraints = false
        breedName.textAlignment = .center
        
        NSLayoutConstraint.activate([
            breedImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            breedImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            breedImage.topAnchor.constraint(equalTo: topAnchor),
            breedImage.heightAnchor.constraint(equalTo: heightAnchor, constant: -40),
            
            breedName.leadingAnchor.constraint(equalTo: leadingAnchor),
            breedName.trailingAnchor.constraint(equalTo: trailingAnchor),
            breedName.topAnchor.constraint(equalTo: breedImage.bottomAnchor),
            breedName.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setupCell(breedImage: UIImage, breedName: String) {
        self.breedImage.image = breedImage
        self.breedName.text =  breedName
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        breedImage.image = nil
        breedName.text = nil
    }
}
