//
//  ViewController.swift
//  SwordHealthChallenge
//
//  Created by Catarina Santos on 31/01/2022.
//

import Foundation
import UIKit

final class CustomTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        UITabBar.appearance().barTintColor = .systemBackground
        tabBar.tintColor = .white
        tabBar.backgroundColor = .black
        setupViewControllers()
    }
    
    fileprivate func createNavController(for rootViewController: UIViewController,
                                         title: String,
                                         image: UIImage) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        navController.navigationBar.prefersLargeTitles = false
        rootViewController.navigationItem.title = title
        return navController
    }
    
    private func setupViewControllers() {
        viewControllers = [
            createNavController(for: BreedsImagesViewController(), title: NSLocalizedString("", comment: ""), image: UIImage(systemName: "house") ?? UIImage()),
            createNavController(for: SearchViewController(), title: NSLocalizedString("", comment: ""), image: UIImage(systemName: "magnifyingglass") ?? UIImage())
        ]
    }
}
